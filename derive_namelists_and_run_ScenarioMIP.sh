#!/bin/bash

CVDP_DIR=`pwd`
CMIP6DIR="/data/CMIP/CMIP6"
CVDP_OUTDIR="/data/aod/cvdp_cmip6/"
#expt_names=`ls $CMIP6DIR/ScenarioMIP/*/* | sort | uniq | grep ssp`
#expt_names=$expt_names" historical" #add historical
expt_names="historical"

for expt in $expt_names
do
  #first set up some run scripts etc for the experiment
  if [ $expt == "historical" ]; then
    OUTDIR=$CVDP_OUTDIR/output.yueming_historical
  else
    OUTDIR=$CVDP_OUTDIR/output.$expt
  fi
  mkdir -p $OUTDIR
  cp driver.ncl $OUTDIR/$expt.driver.ncl
  sed -i "s:output/:$OUTDIR/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:ncl_scripts/:$CVDP_DIR/ncl_scripts/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:Title goes here:$expt:g" $OUTDIR/$expt.driver.ncl
  sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
  if [ $expt != "historical" ]; then
    sed -i "s:climo_syear = 1971:climo_syear = 2070:g" $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_eyear = 2000:climo_eyear = 2099:g" $OUTDIR/$expt.driver.ncl
  fi
  sed -i 's:modular_list = "pdo,aice.trends_timeseries,sst.indices":modular_list = "tas.indices,pr.indices,tas.trends_timeseries,tas.mean_stddev,pr.trends_timeseries,pr.mean_stddev":g' $OUTDIR/$expt.driver.ncl
  sed -i 's:  modular = "False":  modular = "True":g' $OUTDIR/$expt.driver.ncl
  sed -i 's:max_num_tasks = 4:max_num_tasks = 6:g' $OUTDIR/$expt.driver.ncl
  rm -f $OUTDIR/namelist

  # And now start filling the namelist
  cd $CMIP6DIR
  if [ $expt == "historical" ]; then  
    datasets=`ls -d CMIP/*/*/$expt/r?i?p?f?/Amon`
  else
    datasets=`ls -d ScenarioMIP/*/*/$expt/r?i?p?f?/Amon`
  fi
  for dataset in $datasets
  do
    model=`echo $dataset | cut -d/ -f 3`
    ensmem=`echo $dataset | cut -d/ -f 5`
    model_dirstr=`echo "$CMIP6DIR/$dataset/{tas,pr}/g*/v*/"`
    #The harder bits are finding the correct years. NOTE THIS MAY FAIL FOR COMPLETELY EMPTY DATASETS
    ncfiles=`eval ls $model_dirstr*.nc 2> /dev/null`
    let start_yr=9999
    let end_yr=1
    for ncfile_wdir in $ncfiles
    do
      ncfile=${ncfile_wdir##*/}	
      yr_str=`echo $ncfile | cut -d_ -f 7`
      this_start_yr=`echo $yr_str | cut -d- -f 1 | cut -c-4`
      this_end_yr=`echo $yr_str | cut -d- -f 2 | cut -d. -f 1 | cut -c-4`
      if [ $this_start_yr -lt $start_yr ]; then start_yr=$this_start_yr; fi
      if [ $this_end_yr -gt $end_yr ]; then end_yr=$this_end_yr; fi
    done
    let length=$((10#$end_yr))-$((10#$start_yr))
    if [ $length -gt 10 ]; then
	echo "$model $ensmem $expt | $model_dirstr | $start_yr | $end_yr" >> $OUTDIR/namelist
    fi
  done
  cd $CVDP_DIR
done

for expt in $expt_names
do
  OUTDIR=$CVDP_OUTDIR/output.$expt
  cd $OUTDIR
  ncl -n $expt.driver.ncl >& out.log &
done  
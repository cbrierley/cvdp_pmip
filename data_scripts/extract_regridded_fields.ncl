;This script has been designed to make the files for use in exercises at the Climate Model Simulation Summer School at the ITP in Beijing, 2018.
;The paths are such that it should be run from the main directory with ncl -n data_scripts/extract_ipcc_timeseries_to_netcdf.ncl

;Set up paths
data_dir="/data/p2f/cvdp_data"
load "data_scripts/cvdp_data.functions.ncl"

var_names=(/"tas_spatialmean_ann","tas_spatialmean_djf","tas_spatialmean_mam","tas_spatialmean_jja","tas_spatialmean_son",\
            "tas_spatialstddev_ann","tas_spatialstddev_djf","tas_spatialstddev_mam","tas_spatialstddev_jja","tas_spatialstddev_son",\
            "pr_spatialmean_ann","pr_spatialmean_djf","pr_spatialmean_mam","pr_spatialmean_jja","pr_spatialmean_son",\
            "pr_spatialstddev_ann","pr_spatialstddev_djf","pr_spatialstddev_mam","pr_spatialstddev_jja","pr_spatialstddev_son"/) ; the CVDP variable names

ncfiles=systemfunc("ls "+data_dir+"/*.cvdp_data.[0-9]*-*[0-9].nc")

do i=0,dimsizes(ncfiles)-1
  print((/ncfiles(i)/))
  infile=addfile(ncfiles(i),"r")
  outfile_name=str_sub_str(ncfiles(i),".nc",".regridded_1x1_fields.nc")
  system("if [ -f "+outfile_name+" ]; then rm "+outfile_name+"; fi")
  outfile=addfile(outfile_name,"c")
  copy_VarAtts(infile,outfile)
  do var_i=0,dimsizes(var_names)-1 
    if isfilevar(infile,var_names(var_i)) then
       var=read_latlon_var(ncfiles(i),var_names(var_i),True)
       outfile->$var_names(var_i)$=var    
       delete(var)
    end if
  end do
  delete([/infile,outfile,outfile_name/])
end do



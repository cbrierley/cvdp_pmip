load "cvdp_data.functions.ncl"
load "scatter_plot_procedure.ncl"

data_dir="/home/p2f-v/public_html/PMIPVarData/cvdp_data"
plot_dir="../plots"

;Dependent variable
Y_VARNAME="ipcc_CAS_tas"
Y_VARNAME@STAT="AnnCycAmp"; mean, stddev, skew, kurtosis, pcvar
Y_VARNAME@ABS=True; determines whether to set ts_opt@make_absolute=True
Y_VARNAME@SEASON="DJF"
Y_VARNAME@PCT_CHANGES=False
Y_VARNAME@EXPT="midHolocene" ; select a particular experiment; "all" or "missing" selects all experiments
Y_VARNAME@OUTPUT_TYPE="pdf"

;Independent variable
X_VARNAME="climate_sensitivity"         
X_VARNAME@STAT="mean"
X_VARNAME@ABS=True
X_VARNAME@SEASON="DJF"
X_VARNAME@PCT_CHANGES=False
X_VARNAME@EXPT="midHolocene"
X_VARNAME@pdf=True
scatter_plot(X_VARNAME,Y_VARNAME,data_dir,plot_dir,False,False)
delete(X_VARNAME@pdf)

;Dependent variable
Y_VARNAME="ipcc_SAH_pr"
Y_VARNAME@STAT="mean"; mean, stddev, skew, kurtosis, pcvar
Y_VARNAME@ABS=True; determines whether to set ts_opt@make_absolute=True
Y_VARNAME@SEASON="ANN"
Y_VARNAME@PCT_CHANGES=False
Y_VARNAME@EXPT="midHolocene" ; select a particular experiment; "all" or "missing" selects all experiments

scatter_plot(X_VARNAME,Y_VARNAME,data_dir,plot_dir,False,True)

Y_VARNAME="ipcc_WAF_pr"
scatter_plot(X_VARNAME,Y_VARNAME,data_dir,plot_dir,False,True)

;Dependent variable
X_VARNAME="ipcc_CAS_tas"
X_VARNAME@STAT="AnnCycAmp"; mean, stddev, skew, kurtosis, pcvar
X_VARNAME@ABS=True; determines whether to set ts_opt@make_absolute=True
X_VARNAME@SEASON="DJF"
X_VARNAME@PCT_CHANGES=False
X_VARNAME@EXPT="midHolocene" ; select a particular experiment; "all" or "missing" selects all experiments

;Independent variable
Y_VARNAME="ipcc_CAS_tas"         
Y_VARNAME@STAT="mean"
Y_VARNAME@ABS=True
Y_VARNAME@SEASON="ANN"
Y_VARNAME@PCT_CHANGES=False
Y_VARNAME@EXPT="abrupt4xCO2"
scatter_plot(X_VARNAME,Y_VARNAME,data_dir,plot_dir,False,True)


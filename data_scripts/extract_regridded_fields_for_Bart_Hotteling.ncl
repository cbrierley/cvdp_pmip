;This script has been designed to make the files for use in exercises at the Climate Model Simulation Summer School at the ITP in Beijing, 2018.
;The paths are such that it should be run from the main directory with ncl -n data_scripts/extract_ipcc_timeseries_to_netcdf.ncl

;Set up paths
data_dir="/home/p2f-v/public_html/PMIPVarData/cvdp_data"
out_data_dir="/home/ucfaccb/Documents/local_repos/PMIP4-midHolocene/data/for_Bart/"
load "data_scripts/cvdp_data.functions.ncl"

var_names=(/"tas_spatialmean_ann","tas_spatialmean_djf","tas_spatialmean_mam","tas_spatialmean_jja","tas_spatialmean_son",\
            "pr_spatialmean_ann","pr_spatialmean_djf","pr_spatialmean_mam","pr_spatialmean_jja","pr_spatialmean_son",\
            "tas_coldestmonth","tas_warmestmonth","nino34_spacomp_tas_djf1"/) ; the CVDP variable names

ncfiles=systemfunc("ls "+data_dir+"/*{midHolocen*,piControl}.cvdp_data.[0-9]*-*[0-9].nc")

avoidance=(/"ACCESS-ESM1-5","AWI-ESM-old","CNRM-CM6-1","COSMOS-ASO","HadCM3","LOVECLIM"/)

do i=0,dimsizes(ncfiles)-1
  skipper=False
  do j=0,dimsizes(avoidance)-1
    if isStrSubset( ncfiles(i), avoidance(j)) then
       skipper=True
    end if
  end do
  if .not.skipper then
    print((/ncfiles(i)/))
    infile=addfile(ncfiles(i),"r")
    outfile_name=str_sub_str(ncfiles(i),".nc",".regridded_1x1_fields.nc")
    outfile_name=str_sub_str(ncfiles(i),data_dir,out_data_dir)
    system("if [ -f "+outfile_name+" ]; then rm "+outfile_name+"; fi")
    outfile=addfile(outfile_name,"c")
    copy_VarAtts(infile,outfile)
    do var_i=0,dimsizes(var_names)-1 
      if isfilevar(infile,var_names(var_i)) then
         var=read_latlon_var(ncfiles(i),var_names(var_i),True)
         outfile->$var_names(var_i)$=var    
         delete(var)
      end if
    end do
    delete([/infile,outfile,outfile_name/])
  end if
end do



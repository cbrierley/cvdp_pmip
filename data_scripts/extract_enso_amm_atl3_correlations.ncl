;This script was used to create the supplementary table for Brierley & Wainer (2018?) - although that was then tidied up in Excel to make it presentable
;It cycles through all the files in a "data" directory and calculates the standard devations of the timeseries and their correlations/regressions

; run with:
; ncl -n data_scripts/extract_enso_amm_atl3_correlations.ncl > plots/enso_amm_atl3_correlations.csv

load "data_scripts/cvdp_data.functions.ncl"

data_dir="data"

filenames=systemfunc("ls "+data_dir+"/*.cvdp_data.*-*.nc")

print(" ")
print("FILENAME,NINO34_stddev,AMM_stddev,ATL3_stddev,NINO34vsAMM_correl,NINO34vsAMM_regress,NINO34vsATL3_correl,NINO34vsATL3_regress")

do fil=0,dimsizes(filenames)-1
  this_file=addfile(filenames(fil),"r")
  if isfilevar(this_file,"nino34").and.isfilevar(this_file,"amm_timeseries_mon").and.isfilevar(this_file,"atl3_timeseries_mon") then
    nino34=this_file->nino34
    amm=this_file->amm_timeseries_mon
    atl3=this_file->atl3_timeseries_mon
    strings=new(8,string)
    strings(0)=filenames(fil)
    strings(1)=stddev(nino34)
    strings(2)=stddev(amm)
    strings(3)=stddev(atl3)
    strings(4)=escorc(nino34,amm)
    strings(5)=regline(nino34,amm)
    strings(6)=escorc(nino34,atl3)
    strings(7)=regline(nino34,atl3)
    print(str_join(strings, ","))
    delete([/nino34,amm,atl3,strings/])
  end if;isfilevar
end do ;loop over data files 

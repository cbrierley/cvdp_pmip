#!/bin/bash
#This script will curate all the data for access via the webpage.
#Initially it will build a directory and link to all the files. A known structure will further code distribution.
#Presumes all experiments have been run first.

expt_names="piControl abrupt4xCO2 1pctCO2 historical lgm midHolocene lig127k lgm-cal-adj midHolocene-cal-adj lig127k-cal-adj past1000 rcp26 rcp85"
CVDP_DIR=`pwd`
ESGF_DIR="/data/CMIP/curated_ESGF_replica"
CVDP_OUTDIR="/home/p2f-v/public_html/CVDP/"
CVDP_DATA_DIR="/home/p2f-v/public_html/PMIPVarData/cvdp_data/"

cd $ESGF_DIR
gcms=`ls -d */ | cut -d/ -f1`

cd $CVDP_DATA_DIR
ncfiles=`ls $CVDP_OUTDIR/*/output/*.cvdp_data.{?,???,????}-*nc`
for fil in $ncfiles
do
  ln -s $fil ${fil##*/}
done

cd $CVDP_DIR
data_scripts=`ls data_scripts/*.ncl | cut -d/ -f2 | cut -d. -f1`

mkdir -p ~/public_html/PMIPVarData/images
shopt -s extglob #set extra globbing on in script

##Create directory style webpages
#start with gcms
sed "s:sed_string:Climate Model:g" webpage_top.html > ~/public_html/PMIPVarData/gcms.html
for gcm in $gcms
do
  ln -s $CVDP_OUTDIR/individual_gcms/$gcm/ ~/public_html/PMIPVarData/$gcm
  echo "<a href=http://www2.geog.ucl.ac.uk/~p2f-v/PMIPVarData/$gcm class=contenttype-folder state-published url><h4>$gcm</h4></a>" >> ~/public_html/PMIPVarData/gcms.html
done
cd $CVDP_DIR
cat webpage_bottom.html >> ~/public_html/PMIPVarData/gcms.html

#Move onto expts webpage
sed "s:sed_string:Experiment:g" webpage_top.html > ~/public_html/PMIPVarData/expts.html
for expt in $expt_names
do
  ln -s $CVDP_OUTDIR/output.$expt ~/public_html/PMIPVarData/output.$expt
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/output.$expt class=contenttype-folder state-published url><h4>$expt</h4></a>" >> ~/public_html/PMIPVarData/expts.html
done
cd $CVDP_DIR
cat webpage_bottom.html >> ~/public_html/PMIPVarData/expts.html

#Move onto data webpage. Populate both that and the output directory simultaneously
sed "s:sed_string:Output Data and Post-processing Scripts:g" webpage_top.html > ~/public_html/PMIPVarData/data.html
echo "Personally I expect people to download one of the combined files below. These contain all the data for PMIP3/4 and some scripts to plot them. They are about 2Gb and there is a Mac/Linux one (tar.gz) and a Windows one (zip). Alternatively you can download any of the simulation output files or scripts below. All of the data files are on each model's native atmosphere grid. The attached scripts regrid everything onto a 1x1 grid before computation." >> ~/public_html/PMIPVarData/data.html
echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/all_PMIP_cvdp_data.tar.gz class=contenttype-folder state-published url><h3>All Output Data (in tar.gz format)<h3></a>" >> ~/public_html/PMIPVarData/data.html
echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/all_PMIP_cvdp_data.zip class=contenttype-folder state-published url><h3>All Output Data (in zip format)<h3></a>" >> ~/public_html/PMIPVarData/data.html
#copy over some analysis codes
echo "<br>" >> ~/public_html/PMIPVarData/data.html
echo "<h3>NCL Post-Processing Scripts<h3>" >> ~/public_html/PMIPVarData/data.html
echo "These scripts are written in the language of NCL. This is a niche language that has been developed particularly for climate science purposes. Instructions on how to download it are <a href=https://www.ncl.ucar.edu/Download/>here</a>. You can edit the scripts themselves in any simple text editor (e.g. Textedit on Mac or WordPad on Windows) and run from the terminal by typing <tt>ncl ncl_scripts/scriptname.ncl</tt> from the directory that you've downloaded." >> ~/public_html/PMIPVarData/data.html
for data_script in $data_scripts
do
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/code/$data_script.ncl class=contenttype-folder state-published url><h4>$data_script</h4></a>" >> ~/public_html/PMIPVarData/data.html
done
echo "<br>" >> data.html
echo "<h3>Individual Simulation Output<h3>" >> data.html

ln -s $CVDP_DATA_DIR ~/public_html/PMIPVarData/data
ln -s $CVDP_DIR/data_scripts ~/public_html/PMIPVarData/code
cd $CVDP_DATA_DIR
# find the data.nc for all of the experiments
for filename in *.nc
 do
  echo "<a href=http://www2.geog.ucl.ac.uk/~ucfaccb/PMIPVarData/data/$filename class=contenttype-folder state-published url><h4>$filename</h4></a>" >> data.html
done

#create single tar.gz  file
cd ~/public_html/PMIPVarData
tar -chzf all_PMIP_cvdp_data.tar.gz data/*.nc code/*.ncl
zip all_PMIP_cvdp_data.zip data/*.nc code/*.ncl

#Finish off data webpage
cd $CVDP_DIR
cat webpage_bottom.html >> data.html

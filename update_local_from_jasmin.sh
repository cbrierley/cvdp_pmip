#!/bin/bash

if [[ $(hostname) = *.badc.* ]] || [[ $(hostname) = *.rl.* ]]; then
  echo "This script should be run from UCL"
else
    exec ssh-agent $SHELL
    ssh-add ~/.ssh/id_rsa_jasmin
    expt_names="piControl abrupt4xCO2 1pctCO2 historical lgm midHolocene past1000 rcp85"
    for expt in $expt_names
    do
        rsync -avz ucfaccb@jasmin-xfer1.ceda.ac.uk:/group_workspaces/jasmin2/ncas_generic/users/ucfaccb/cvdp_cmip5/output.$expt /data/aod/cvdp_cmip5/jasmin_mirror
    done
fi

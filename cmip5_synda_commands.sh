#!/bin/bash

sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.piControl.mon.atmos.Amon.r1i1p1.v1 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.piControl.mon.seaIce.OImon.r1i1p1.v1 OImon sic
sudo synda install -y BCC midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y BCC midHolocene r1i1p1 OImon sic
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.historical.mon.atmos.Amon.r1i1p1.v1 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.historical.mon.seaIce.OImon.r1i1p1.v1 sic
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.rcp85.mon.atmos.Amon.r1i1p1.v20120705 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.rcp85.mon.seaIce.OImon.r1i1p1.v20120705 sic
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.1pctCO2.mon.atmos.Amon.r1i1p1.v1 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.1pctCO2.mon.seaIce.OImon.r1i1p1.v1 sic
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.abrupt4xCO2.mon.atmos.Amon.r1i1p1.v1 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.abrupt4xCO2.mon.seaIce.OImon.r1i1p1.v1 sic
sudo synda install -y cmip5.output1.BCC.bcc-csm1-1.past1000.mon.atmos.Amon.r1i1p1.v20120606 variable=ts,tas,psl,pr
sudo synda install -y BCC past1000 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 piControl r1i1p1 OImon sic
sudo synda install -y model=CCSM4 piControl r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 midHolocene r1i1p1 OImon sic
sudo synda install -y model=CCSM4 midHolocene r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 midHolocene r2i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 midHolocene r2i1p1 OImon sic
sudo synda install -y model=CCSM4 midHolocene r2i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 lgm r1i1p1 OImon sic
sudo synda install -y model=CCSM4 lgm r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 lgm r2i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 lgm r2i1p1 OImon sic
sudo synda install -y cmip5.output1.NCAR.CCSM4.lgm.mon.ocean.Omon.r2i1p1.v20140820 msftmyz 
sudo synda install -y cmip5.output1.NCAR.CCSM4.historical.mon.atmos.Amon.r1i1p1.v20160829 variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 historical r1i1p1 OImon sic
sudo synda install -y cmip5.output1.NCAR.CCSM4.historical.mon.ocean.Omon.r1i1p1.v20140820 msftmyz
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y model=CCSM4 past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 past1000 r1i1p1 OImon sic
sudo synda install -y model=CCSM4 past1000 r1i1p1 Omon msftmyz
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.atmos.Amon.r1i1p1.v20160829 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.seaIce.OImon.r1i1p1.v20130228 sic
sudo synda install -y cmip5.output1.NCAR.CCSM4.rcp85.mon.ocean.Omon.r1i1p1.v20140820 msftmyz
sudo synda install -y model=CCSM4 abrupt4xCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CCSM4 1pctCO2 r1i1p1 OImon sic
sudo synda install -y cmip5.output1.NCAR.CCSM4.abrupt4xCO2.mon.ocean.Omon.r1i1p1.v20140820 msftmyz
sudo synda install -y CNRM-CM5 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CNRM-CM5 piControl r1i1p1 OImon sic
sudo synda install -y CNRM-CM5 piControl r1i1p1 Omon msftmyz
sudo synda install -y CNRM-CM5 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CNRM-CM5 midHolocene r1i1p1 OImon sic
sudo synda install -y CNRM-CM5 midHolocene r1i1p1 Omon msftmyz
sudo synda install -y CNRM-CM5 lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CNRM-CM5 lgm r1i1p1 OImon sic
sudo synda install -y CNRM-CM5 lgm r1i1p1 Omon msftmyz
sudo synda install -y CNRM-CM5 historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CNRM-CM5 historical r1i1p1 OImon sic
sudo synda install -y CNRM-CM5 historical r1i1p1 Omon msftmyz
sudo synda install -y CNRM-CM5 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CNRM-CM5 1pctCO2 r1i1p1 OImon sic
sudo synda install -y CNRM-CM5 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y model=CNRM-CM5 rcp85 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CNRM-CM5 rcp85 r1i1p1 OImon sic
sudo synda install -y model=CNRM-CM5 rcp85 r1i1p1 Omon msftmyz
sudo synda install -y model=CNRM-CM5 abrupt4xCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y model=CNRM-CM5 1pctCO2 r1i1p1 OImon sic
sudo synda install -y model=CNRM-CM5 abrupt4xCO2 r1i1p1 Omon msftmyz
sudo synda install -y COSMOS-ASO piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y COSMOS-ASO lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y COSMOS-ASO lgm r1i1p1 OImon sic
sudo synda install -y cmip5.output1.CSIRO-QCCCE.CSIRO-Mk3-6-0.piControl.mon.atmos.Amon.r1i1p1.v20120607 variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE piControl r1i1p1 OImon sic
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE midHolocene r1i1p1 OImon sic
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE historical r1i1p1 OImon sic
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y CSIRO-Mk3-6-0 CSIRO-QCCCE 1pctCO2 r1i1p1 OImon sic
sudo synda install -y UNSW Amon variable=ts,tas,psl,pr
sudo synda install -y UNSW OImon sic
sudo synda install -y EC-EARTH-2-2 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y EC-EARTH-2-2 piControl r1i1p1 OImon sic
sudo synda install -y EC-EARTH-2-2 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y EC-EARTH-2-2 midHolocene r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 piControl r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 piControl r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 midHolocene r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 midHolocene r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 lgm r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 lgm r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 historical r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 historical r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 rcp85 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 rcp85 r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 rcp85 r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-g2 1pctCO2 r1i1p1 OImon sic
sudo synda install -y FGOALS-g2 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-g2 abrupt4xCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.LASG-CESS.FGOALS-g2.abrupt4xCO2.mon.seaIce.OImon.r1i1p1.v1 sic
sudo synda install -y FGOALS-g2 abrupt4xCO2 r1i1p1 Omon msftmyz
sudo synda install -y FGOALS-gl past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-s2 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-s2 piControl r1i1p1 OImon sic
sudo synda install -y FGOALS-s2 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-s2 midHolocene r1i1p1 OImon sic
sudo synda install -y FGOALS-s2 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y FGOALS-s2 1pctCO2 r1i1p1 OImon sic
sudo synda install -y cmip5.output1.LASG-IAP.FGOALS-s2.abrupt4xCO2.mon.atmos.Amon.r1i1p1.v1 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.LASG-IAP.FGOALS-s2.abrupt4xCO2.mon.seaIce.OImon.r1i1p1.v2 sic
sudo synda install -y FGOALS-s2 rcp85 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.LASG-IAP.FGOALS-s2.past1000.mon.atmos.Amon.r1i1p1.v20140818 variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R piControl Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R piControl OImon sic
sudo synda install -y GISS-E2-R midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R midHolocene r1i1p1 OImon sic
sudo synda install -y GISS-E2-R lgm Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R lgm OImon sic
sudo synda install -y GISS-E2-R historical r1i1p1,r1i1p2 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R historical r1i1p1,r1i1p2 OImon sic
sudo synda install -y GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R 1pctCO2 r1i1p1,r1i1p2,r1i1p3 OImon sic
sudo synda install -y GISS-E2-R past1000 Amon variable=ts,tas,psl,pr
sudo synda install -y GISS-E2-R past1000 OImon sic
sudo synda install -y HadCM3 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadCM3 piControl r1i1p1 OImon sic
sudo synda install -y HadCM3 past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadCM3 past1000 r1i1p1 OImon sic
sudo synda install -y HadGEM2-CC piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-CC piControl r1i1p1 OImon sic
sudo synda install -y HadGEM2-CC midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-CC midHolocene r1i1p1 OImon sic
sudo synda install -y HadGEM2-CC historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-CC historical r1i1p1 OImon sic
sudo synda install -y cmip5.output1.MOHC.HadGEM2-ES.piControl.mon.atmos.Amon.r1i1p1.v20130114 variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-ES piControl r1i1p1 OImon sic
sudo synda install -y HadGEM2-ES midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-ES midHolocene r1i1p1 OImon sic
sudo synda install -y cmip5.output1.MOHC.HadGEM2-ES.historical.mon.atmos.Amon.r1i1p1.v20120928 variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-ES historical r1i1p1 OImon sic
sudo synda install -y HadGEM2-ES 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y HadGEM2-ES 1pctCO2 r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR piControl r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR midHolocene r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR lgm r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR historical r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR rcp85 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR rcp85 r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR 1pctCO2 r1i1p1 OImon sic
sudo synda install -y IPSL-CM5A-LR past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y IPSL-CM5A-LR past1000 r1i1p1 OImon sic
sudo synda install -y pmip3.output.CAU-GEOMAR.KCM1-2-2.piControl.mon.atmos.Amon.r1i1p1.v20170118 variable=ts,tas,psl,pr
sudo synda install -y pmip3.output.CAU-GEOMAR.KCM1-2-2.midHolocene.mon.atmos.Amon.r1i1p1.v20170118 Amon variable=ts,tas,psl,pr
sudo synda install -y pmip3.output.CAU-GEOMAR.KCM1-2-2.1pctCO2.mon.atmos.Amon.r1i1p1.v2017011 variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM piControl r1i1p1 OImon sic
sudo synda install -y MIROC-ESM midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM midHolocene r1i1p1 OImon sic
sudo synda install -y MIROC-ESM lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM lgm r1i1p1 OImon sic
sudo synda install -y MIROC-ESM historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM historical r1i1p1 OImon sic
sudo synda install -y MIROC-ESM historical r1i1p1 Omon msftmyz
sudo synda install -y MIROC-ESM 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM 1pctCO2 r1i1p1 OImon sic
sudo synda install -y MIROC-ESM past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MIROC-ESM past1000 r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P piControl r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P piControl r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P midHolocene r1i1p2 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P midHolocene r1i1p2 OImon sic
sudo synda install -y MPI-ESM-P midHolocene r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P midHolocene r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P midHolocene r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P lgm r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P lgm r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P lgm r1i1p2 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P lgm r1i1p2 OImon sic
sudo synda install -y MPI-ESM-P lgm r1i1p2 Omon msftmyz
sudo synda install -y MPI-ESM-P historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P historical r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P historical r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P 1pctCO2 r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P 1pctCO2 r1i1p1 Omon msftmyz
sudo synda install -y MPI-ESM-P past1000 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MPI-ESM-P past1000 r1i1p1 OImon sic
sudo synda install -y MPI-ESM-P past1000 r1i1p1 Omon msftmyz
sudo synda install -y MRI-CGCM3 piControl r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MRI-CGCM3 piControl r1i1p1 OImon sic
sudo synda install -y MRI-CGCM3 piControl r1i1p1 Omon msftmyz
sudo synda install -y MRI-CGCM3 midHolocene r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MRI-CGCM3 midHolocene r1i1p1 OImon sic
sudo synda install -y MRI-CGCM3 midHolocene r1i1p1 Omon msftmyz
sudo synda install -y MRI-CGCM3 lgm r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MRI-CGCM3 lgm r1i1p1 OImon sic
sudo synda install -y MRI-CGCM3 lgm r1i1p1 Omon msftmyz
sudo synda install -y MRI-CGCM3 historical r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MRI-CGCM3 historical r1i1p1 OImon sic
sudo synda install -y MRI-CGCM3 historical r1i1p1 Omon msftmyz
sudo synda install -y MRI-CGCM3 1pctCO2 r1i1p1 Amon variable=ts,tas,psl,pr
sudo synda install -y MRI-CGCM3 1pctCO2 r1i1p1 OImon sic
sudo synda install -y MRI-CGCM3 1pctCO2 r1i1p1 Omon msftmyz
# This commands download some overlapping data, so remove the offenders
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.psl_Amon_FGOALS-g2_historical_r1i1p1_201001-201412.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.psl_Amon_FGOALS-g2_historical_r1i1p1_200001-200912.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.ts_Amon_FGOALS-g2_historical_r1i1p1_200001-200912.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.ts_Amon_FGOALS-g2_historical_r1i1p1_201001-201412.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.pr_Amon_FGOALS-g2_historical_r1i1p1_201001-201412.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.pr_Amon_FGOALS-g2_historical_r1i1p1_200001-200912.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.tas_Amon_FGOALS-g2_historical_r1i1p1_200001-200912.nc
sudo synda remove -y cmip5.output1.LASG-CESS.FGOALS-g2.historical.mon.atmos.Amon.r1i1p1.v1.tas_Amon_FGOALS-g2_historical_r1i1p1_201001-201412.nc

echo `ls /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/atmos/Amon/r1i1p1/v20121017/*_Amon_GISS-E2-R_piControl_r1i1p1_3[3456]*.nc` > files_to_remove
sed -i 's:/data/CMIP/cmip5:cmip5:g' files_to_remove
sed -i 's:/:.:g' files_to_remove
files=`cat files_to_remove`
for fil in $files
do 
   sudo synda remove -y $fil
done
rm $files

#Need to download some additonal files from abrupt4xCO2
sudo synda install -y cmip5.output1.CSIRO-QCCCE.CSIRO-Mk3-6-0.abrupt4xCO2.mon.atmos.Amon.r1i1p1.v20120323 variable=ts,tas,psl,pr
sudo synda install -y cmip5.output1.CSIRO-QCCCE.CSIRO-Mk3-6-0.abrupt4xCO2.mon.seaIce.OImon.r1i1p1.v20120323 OImon sic


# alter the HadGEM2 files to not start in December...
ncrcat -O -d time,'1860-01-01','1999-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/1pctCO2/mon/seaIce/OImon/r1i1p1/v20111017/sic_OImon_HadGEM2-ES_1pctCO2_r1i1p1_1* /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/sic_OImon_HadGEM2-ES_1pctCO2_r1i1p1_186001-199912.nc
ncrcat -O -d time,'1860-01-01','1999-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/1pctCO2/mon/atmos/Amon/r1i1p1/v20110330/pr_Amon_HadGEM2-ES_1pctCO2_r1i1p1_1* /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/pr_Amon_HadGEM2-ES_1pctCO2_r1i1p1_186001-199912.nc
ncrcat -O -d time,'1860-01-01','1999-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/1pctCO2/mon/atmos/Amon/r1i1p1/v20110330/ts_Amon_HadGEM2-ES_1pctCO2_r1i1p1_1* /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/ts_Amon_HadGEM2-ES_1pctCO2_r1i1p1_186001-199912.nc
ncrcat -O -d time,'1860-01-01','1999-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/1pctCO2/mon/atmos/Amon/r1i1p1/v20110330/psl_Amon_HadGEM2-ES_1pctCO2_r1i1p1_1* /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/psl_Amon_HadGEM2-ES_1pctCO2_r1i1p1_186001-199912.nc
ncrcat -O -d time,'1860-01-01','1999-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/1pctCO2/mon/atmos/Amon/r1i1p1/v20110330/tas_Amon_HadGEM2-ES_1pctCO2_r1i1p1_1* /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/tas_Amon_HadGEM2-ES_1pctCO2_r1i1p1_186001-199912.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/historical/mon/atmos/Amon/r1i1p1/v20120928/ts/ts_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/historical/ts_Amon_HadGEM2-ES_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/historical/mon/atmos/Amon/r1i1p1/v20120928/tas/tas_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/historical/tas_Amon_HadGEM2-ES_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/historical/mon/atmos/Amon/r1i1p1/v20120928/pr/pr_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/historical/pr_Amon_HadGEM2-ES_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/historical/mon/atmos/Amon/r1i1p1/v20120928/psl/psl_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/historical/psl_Amon_HadGEM2-ES_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/historical/mon/seaIce/OImon/r1i1p1/v20110916/sic_OImon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/historical/sic_OImon_HadGEM2-ES_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'2061-01-01','2161-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/ts_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/ts_Amon_HadGEM2-ES_midHolocene_r1i1p1_206101-216112.nc
ncrcat -O -d time,'2061-01-01','2161-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/tas_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/tas_Amon_HadGEM2-ES_midHolocene_r1i1p1_206101-216112.nc
ncrcat -O -d time,'2061-01-01','2161-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/pr_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/pr_Amon_HadGEM2-ES_midHolocene_r1i1p1_206101-216112.nc
ncrcat -O -d time,'2061-01-01','2161-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/psl_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/psl_Amon_HadGEM2-ES_midHolocene_r1i1p1_206101-216112.nc
ncrcat -O -d time,'2061-01-01','2161-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/midHolocene/mon/seaIce/OImon/r1i1p1/v20120222/sic_OImon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/sic_OImon_HadGEM2-ES_midHolocene_r1i1p1_206101-216112.nc
ncrcat -O -d time,'1860-01-01','2434-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/piControl/mon/atmos/Amon/r1i1p1/v20110524/ts_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/piControl/ts_Amon_HadGEM2-ES_piControl_r1i1p1_186001-243412.nc
ncrcat -O -d time,'1860-01-01','2434-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/piControl/mon/atmos/Amon/r1i1p1/v20110524/tas_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/piControl/tas_Amon_HadGEM2-ES_piControl_r1i1p1_186001-243412.nc
ncrcat -O -d time,'1860-01-01','2434-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/piControl/mon/atmos/Amon/r1i1p1/v20110524/pr_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/piControl/pr_Amon_HadGEM2-ES_piControl_r1i1p1_186001-243412.nc
ncrcat -O -d time,'1860-01-01','2434-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/piControl/mon/atmos/Amon/r1i1p1/v20110524/psl_Amon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/piControl/psl_Amon_HadGEM2-ES_piControl_r1i1p1_186001-243412.nc
ncrcat -O -d time,'1860-01-01','2434-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-ES/piControl/mon/seaIce/OImon/r1i1p1/v20111222/sic_OImon_HadGEM2-ES_*.nc /data/aod/input_for_cvdp/HadGEM2-ES/piControl/sic_OImon_HadGEM2-ES_piControl_r1i1p1_186001-243412.nc
ncrcat -O -d time,'1860-01-01','2099-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/piControl/mon/atmos/Amon/r1i1p1/v20111109/ts_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/piControl/ts_Amon_HadGEM2-CC_piControl_r1i1p1_186001-209912.nc
ncrcat -O -d time,'1860-01-01','2099-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/piControl/mon/atmos/Amon/r1i1p1/v20111109/tas_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/piControl/tas_Amon_HadGEM2-CC_piControl_r1i1p1_186001-209912.nc
ncrcat -O -d time,'1860-01-01','2099-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/piControl/mon/atmos/Amon/r1i1p1/v20111109/pr_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/piControl/pr_Amon_HadGEM2-CC_piControl_r1i1p1_186001-209912.nc
ncrcat -O -d time,'1860-01-01','2099-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/piControl/mon/atmos/Amon/r1i1p1/v20111109/psl_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/piControl/psl_Amon_HadGEM2-CC_piControl_r1i1p1_186001-209912.nc
ncrcat -O -d time,'1860-01-01','2099-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/piControl/mon/seaIce/OImon/r1i1p1/v20111109/sic_OImon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/piControl/sic_OImon_HadGEM2-CC_piControl_r1i1p1_186001-209912.nc
ncrcat -O -d time,'1942-01-01','1976-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/ts_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/ts_Amon_HadGEM2-CC_midHolocene_r1i1p1_194201-197612.nc
ncrcat -O -d time,'1942-01-01','1976-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/tas_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/tas_Amon_HadGEM2-CC_midHolocene_r1i1p1_194201-197612.nc
ncrcat -O -d time,'1942-01-01','1976-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/pr_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/pr_Amon_HadGEM2-CC_midHolocene_r1i1p1_194201-197612.nc
ncrcat -O -d time,'1942-01-01','1976-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/midHolocene/mon/atmos/Amon/r1i1p1/v20120222/psl_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/psl_Amon_HadGEM2-CC_midHolocene_r1i1p1_194201-197612.nc
ncrcat -O -d time,'1941-01-01','1975-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/midHolocene/mon/seaIce/OImon/r1i1p1/v20120223/sic_OImon_HadGEM2-CC_midHolocene_r1i1p1_194012-197512.nc /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/sic_OImon_HadGEM2-CC_midHolocene_r1i1p1_194201-197612.nc

ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/historical/mon/atmos/Amon/r1i1p1/v20110927/ts_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/historical/ts_Amon_HadGEM2-CC_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/historical/mon/atmos/Amon/r1i1p1/v20110927/tas_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/historical/tas_Amon_HadGEM2-CC_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/historical/mon/atmos/Amon/r1i1p1/v20110927/pr_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/historical/pr_Amon_HadGEM2-CC_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/historical/mon/atmos/Amon/r1i1p1/v20110927/psl_Amon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/historical/psl_Amon_HadGEM2-CC_historical_r1i1p1_186001-200412.nc
ncrcat -O -d time,'1860-01-01','2004-12-31' /data/CMIP/cmip5/output1/MOHC/HadGEM2-CC/historical/mon/seaIce/OImon/r1i1p1/v20110930/sic_OImon_HadGEM2-CC_*.nc /data/aod/input_for_cvdp/HadGEM2-CC/historical/sic_OImon_HadGEM2-CC_historical_r1i1p1_186001-200412.nc

# Make the C20_Reanalysis $OUTDIR/namelist...
NAME="C20-Reanalysis"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "C20-Reanalysis | /home/ucfaccb/DATA/obs/C20_Reanal/ | 1871 | 2012" > $OUTDIR/namelist

# Make the bcc-csm1-1 $OUTDIR/namelist...
NAME="bcc-csm1-1"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "bcc-csm1-1 piControl | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v1/{pr,tas,ts,psl,sic,}/ | 001 | 050 " > $OUTDIR/namelist 
echo "bcc-csm1-1 midHolocene | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{pr,tas,ts,psl,sic,}/ | 001 | 100" >> $OUTDIR/namelist
echo "bcc-csm1-1 historical | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{pr,tas,ts,psl,sic,}/ | 1850 | 2012" >> $OUTDIR/namelist
echo "bcc-csm1-1 1pctCO2 | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{pr,tas,ts,psl,sic,}/ | 160 | 299 " >> $OUTDIR/namelist
echo "bcc-csm1-1 past1000 | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20120606/{pr,tas,ts,psl,sic,}/ | 850 | 1850 " >> $OUTDIR/namelist
echo "bcc-csm1-1 abrupt4xCO2 | /data/CMIP/cmip5/output1/BCC/bcc-csm1-1/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{pr,tas,ts,psl,sic,}/ | 160 | 309 " >> $OUTDIR/namelist

# Make the CCSM4 $OUTDIR/namelist...
NAME="CCSM4"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "CCSM4 piControl | /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20121128,v20130510,v20130514}/ | 250 | 1300" > $OUTDIR/namelist 
echo "CCSM4-r2 piControl | /data/CMIP/cmip5/output1/NCAR/CCSM4/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20121128,v20130510,v20130514}/ | 250 | 1300" > $OUTDIR/namelist 
echo "CCSM4 midHolocene | /data/CMIP/cmip5/output1/NCAR/CCSM4/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20120524,v20121128}/ | 1000 | 1300" >> $OUTDIR/namelist
echo "CCSM4-r2 midHolocene | /data/CMIP/cmip5/output1/NCAR/CCSM4/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r2i1p1/{v20120604,v20120529,v20121128}/ | 1269 | 1300" >> $OUTDIR/namelist
echo "CCSM4 0 lgm | /data/CMIP/cmip5/output1/NCAR/CCSM4/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120203,v20120524,v20121128}/ | 1800 | 1900" >> $OUTDIR/namelist
echo "CCSM4-r2 lgm | /data/CMIP/cmip5/output1/NCAR/CCSM4/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r2i1p1/{v20120604,v20120524,v20140820}/ | 1870 | 1900" >> $OUTDIR/namelist
echo "CCSM4 0 historical | /data/CMIP/cmip5/output1/NCAR/CCSM4/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20160829,v20120202,v20121128,v20140820}/{ts,tas,psl,pr,msftmyz,}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "CCSM4 0 1pctCO2 | /data/CMIP/cmip5/output1/NCAR/CCSM4/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20121031,v20120529,v20140820}/ | 1850 | 1989" >> $OUTDIR/namelist
echo "CCSM4 0 past1000 | /data/CMIP/cmip5/output1/NCAR/CCSM4/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20120411,v20121128}/ | 850 | 1850" >> $OUTDIR/namelist
echo "CCSM4 0 abrupt4xCO2 | /data/CMIP/cmip5/output1/NCAR/CCSM4/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120604,v20140820}/ | 1850 | 2000" >> $OUTDIR/namelist

# Make the CNRM-CM5 $OUTDIR/namelist...
NAME="CNRM-CM5"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "CNRM-CM5 piControl | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20130101,v20110701,v20121001}/ | 2400 | 2699" > $OUTDIR/namelist 
echo "CNRM-CM5 midHolocene | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110927,v20130101,v20130101}/ | 1950 | 2149" >> $OUTDIR/namelist
echo "CNRM-CM5 lgm | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120530,v20120828,v20120730}/ | 1800 | 1999" >> $OUTDIR/namelist
echo "CNRM-CM5 historical | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110901,v20130101}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "CNRM-CM5 1pctCO2 | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110701,v20130101}/ | 1850 | 1989" >> $OUTDIR/namelist
echo "CNRM-CM5 abrupt4xCO2 | /data/CMIP/cmip5/output1/CNRM-CERFACS/CNRM-CM5/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110701,v20130101}/ | 1850 | 1999" >> $OUTDIR/namelist

NAME="COSMOS-ASO"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "COSMOS-ASO piControl | /data/CMIP/pmip3/output/FUB/COSMOS-ASO/piControl/mon/atmos/Amon/r1i1p1/v20120419/{ts,tas,psl,pr,}/ | 001 | 399 " > $OUTDIR/namelist 
echo "COSMOS-ASO lgm | /data/CMIP/pmip3/output/FUB/COSMOS-ASO/lgm/mon/atmos/{OImon,Amon}/r1i1p1/v20120503/{ts,tas,psl,pr,}/ | 001 | 599" >> $OUTDIR/namelist

# Make the CSIRO-Mk3-6-0 $OUTDIR/namelist...
NAME="CSIRO-Mk3-6-0"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "CSIRO-Mk3-6-0 piControl | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v1,v20120607}/ | 1 | 500" > $OUTDIR/namelist 
echo "CSIRO-Mk3-6-0 midHolocene | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1 | 100" >> $OUTDIR/namelist
echo "CSIRO-Mk3-6-0 historical | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1850 | 2005" >> $OUTDIR/namelist
echo "CSIRO-Mk3-6-0 1pctCO2 | /data/CMIP/cmip5/output1/CSIRO-QCCCE/CSIRO-Mk3-6-0/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/ | 1 | 140" >> $OUTDIR/namelist

# Make the CSIRO-Mk3L-1-2 $OUTDIR/namelist...
NAME="CSIRO-Mk3L-1-2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "CSIRO-Mk3L-1-2 piControl | /data/CMIP/pmip3/output/UNSW/CSIRO-Mk3L-1-2/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120307/ | 1 | 1000" > $OUTDIR/namelist 
echo "CSIRO-Mk3L-1-2 midHolocene | /data/CMIP/pmip3/output/UNSW/CSIRO-Mk3L-1-2/midHolocene/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120307/ | 1 | 500" >> $OUTDIR/namelist
echo "CSIRO-Mk3L-1-2 past1000 | /data/CMIP/pmip3/output/UNSW/CSIRO-Mk3L-1-2/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120307/ | 851 | 1850" >> $OUTDIR/namelist
echo "CSIRO-Mk3L-1-2 1pctCO2 | /data/CMIP/pmip3/output/UNSW/CSIRO-Mk3L-1-2/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120307/ | 1 | 140" >> $OUTDIR/namelist

# Make the EC-EARTH-2-2 $OUTDIR/namelist...
NAME="EC-EARTH-2-2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "EC-EARTH-2-2 piControl | /data/CMIP/pmip3/output/ICHEC/EC-EARTH-2-2/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20130128,v20120712}/{ts,tas,psl,pr,}/ | 1860 | 1899" > $OUTDIR/namelist 
echo "EC-EARTH-2-2 midHolocene | /data/CMIP/pmip3/output/ICHEC/EC-EARTH-2-2/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20131219,v20120715}/{ts,tas,psl,pr,}/ | 1860 | 1899" >> $OUTDIR/namelist

# Make the FGOALS-g2 $OUTDIR/namelist...
NAME="FGOALS-g2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "FGOALS-g2 piControl | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{msftmyz/,ts/,sic/,} | 701 | 900" > $OUTDIR/namelist 
echo "FGOALS-g2 midHolocene | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v1,v20130314}/{msftmyz/,ts/,sic/,} | 801 | 1000" >> $OUTDIR/namelist
echo "FGOALS-g2 lgm | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{msftmyz/,ts/,sic/,} | 550 | 649" >> $OUTDIR/namelist
echo "FGOALS-g2 historical | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{msftmyz/,ts/,sic/,} | 1850 | 2005" >> $OUTDIR/namelist
echo "FGOALS-g2 1pctCO2 | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{msftmyz/,ts/,sic/,} | 440 | 579" >> $OUTDIR/namelist
echo "FGOALS-g2 abrupt4xCO2 | /data/CMIP/cmip5/output1/LASG-CESS/FGOALS-g2/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v1/{msftmyz/,ts/,sic/,} | 490 | 747" >> $OUTDIR/namelist
#NOTE: I am not completely sure about the timings in 1pctC02 or abrupt4xC02 runs. The GMT looks like the EXPT doesn't start in first year 

NAME="FGOALS-gl"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "FGOALS-gl past1000 | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-gl/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v2/{msftmyz/,ts/,sic/,} | 1000 | 1999" >> $OUTDIR/namelist 

NAME="FGOALS-s2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "FGOALS-s2 piControl | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-s2/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v4,v2}/{msftmyz/,ts/,sic/,} | 1850 | 2350" > $OUTDIR/namelist 
echo "FGOALS-s2 midHolocene | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-s2/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v2/{msftmyz/,ts/,sic/,} | 01 | 100" >> $OUTDIR/namelist
echo "FGOALS-s2 1pctCO2 | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-s2/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v1,v2}/{msftmyz/,ts/,sic/,} | 1850 | 1989" >> $OUTDIR/namelist
echo "FGOALS-s2 past1000 | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-s2/past1000/mon/atmos/Amon/r1i1p1/v20140818/{msftmyz/,ts/,sic/,} | 850 | 1850" >> $OUTDIR/namelist
echo "FGOALS-s2 abrupt4xCO2 | /data/CMIP/cmip5/output1/LASG-IAP/FGOALS-s2/abrupt4xCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v1,v2}/{msftmyz/,ts/,sic/,} | 1850 | 1999" >> $OUTDIR/namelist

NAME="GISS-E2-R"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "GISS-E2-R piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20121017/ | 3981 | 4530" >> $OUTDIR/namelist 
echo "GISS-E2-R-p2 piControl | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20121017/ | 3616| 4120" >> $OUTDIR/namelist 
echo "GISS-E2-R midHolocene | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/midHolocene/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120716/ | 2500 | 2599" >> $OUTDIR/namelist
echo "GISS-E2-R lgm | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/lgm/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p150/v20120516/ | 3000 | 3099" >> $OUTDIR/namelist
echo "GISS-E2-R-p2 lgm | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p151/v20120516/ | 3000 | 3099" >> $OUTDIR/namelist
echo "GISS-E2-R historical | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/historical/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20121015/ | 1850 | 2005" >> $OUTDIR/namelist
echo "GISS-E2-R-p2 historical | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/historical/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20121015/ | 1850 | 2005" >> $OUTDIR/namelist
echo "GISS-E2-R 1pctCO2 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120803/ | 1850 | 1989" >> $OUTDIR/namelist
echo "GISS-E2-R-p2 1pctCO2 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p2/v20120803/ | 1850 | 1989" >> $OUTDIR/namelist
echo "GISS-E2-R-p121 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p121/v20120531/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p122 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p122/v20120605/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p123 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p123/v20120907/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p124 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p124/v20120516/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p125 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p125/v20120516/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p126 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p126/v20120913/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p127 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p127/v20120824/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p128 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p128/v20121213/ | 850 | 1850" >> $OUTDIR/namelist
echo "GISS-E2-R-p1221 past1000 | /data/CMIP/cmip5/output1/NASA-GISS/GISS-E2-R/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1221/v20130522/ | 850 | 1850" >> $OUTDIR/namelist

NAME="HadCM3"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "HadCM3 piControl | /data/CMIP/pmip3/output/UOED/HadCM3/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20130313/ | 801 | 1999" > $OUTDIR/namelist 
echo "HadCM3 past1000 | /data/CMIP/pmip3/output/UOED/HadCM3/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20130313/ | 850 | 1850" >> $OUTDIR/namelist

NAME="HadGEM2-CC"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "HadGEM2-CC piControl | /data/aod/input_for_cvdp/HadGEM2-CC/piControl/ | 1860 | 2099" > $OUTDIR/namelist 
echo "HadGEM2-CC midHolocene | /data/aod/input_for_cvdp/HadGEM2-CC/midHolocene/ | 1942 | 1975" >> $OUTDIR/namelist
echo "HadGEM2-CC historical | /data/aod/input_for_cvdp/HadGEM2-CC/historical/ | 1860 | 2004" >> $OUTDIR/namelist

NAME="HadGEM2-ES"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "HadGEM2-ES piControl | /data/aod/input_for_cvdp/HadGEM2-ES/piControl/ | 1860 | 2424" > $OUTDIR/namelist 
echo "HadGEM2-ES midHolocene | /data/aod/input_for_cvdp/HadGEM2-ES/midHolocene/ | 2061 | 2161" >> $OUTDIR/namelist
echo "HadGEM2-ES historical | /data/aod/input_for_cvdp/HadGEM2-ES/historical/ | 1860 | 2004" >> $OUTDIR/namelist
echo "HadGEM2-ES 1pctCO2 | /data/aod/input_for_cvdp/HadGEM2-ES/1pctCO2/ | 1860 | 1999" >> $OUTDIR/namelist
echo "HadGEM2-ES past1000 | /data/aod/input_for_cvdp/HadGEM2-ES/past1000/ | 862 | 1849" >> $OUTDIR/namelist

NAME="IPSL-CM5A-LR"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "IPSL-CM5A-LR piControl | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/{v20130506,v20111010}/ | 1800 | 2799 " > $OUTDIR/namelist 
echo "IPSL-CM5A-LR midHolocene | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20111119/ | 2301 | 2800" >> $OUTDIR/namelist
echo "IPSL-CM5A-LR lgm | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20120114/ | 2601 | 2800" >> $OUTDIR/namelist
echo "IPSL-CM5A-LR historical | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110406,v20111010}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "IPSL-CM5A-LR 1pctCO2 | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20110427,v20111010}/ | 1850 | 1989 " >> $OUTDIR/namelist
echo "IPSL-CM5A-LR past1000 | /data/CMIP/cmip5/output1/IPSL/IPSL-CM5A-LR/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20120804/ | 850 | 1850 " >> $OUTDIR/namelist

NAME="KCM1-2-2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "KCM1-2-2 piControl | /data/CMIP/pmip3/output/CAU-GEOMAR/KCM1-2-2/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20170118/ | 1400 | 1599" > $OUTDIR/namelist 
echo "KCM1-2-2 midHolocene | /data/CMIP/pmip3/output/CAU-GEOMAR/KCM1-2-2/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20170118/ | 1400 | 1499" >> $OUTDIR/namelist
echo "KCM1-2-2 1pctCO2 | /data/CMIP/pmip3/output/CAU-GEOMAR/KCM1-2-2/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/v20170118/ | 1200 | 1332" >> $OUTDIR/namelist

NAME="MRI-CGCM3"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "MRI-CGCM3 piControl | /data/CMIP/cmip5/output1/MRI/MRI-CGCM3/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120701,v20120510}/ | 2001 | 2350" > $OUTDIR/namelist 
echo "MRI-CGCM3 midHolocene | /data/CMIP/cmip5/output1/MRI/MRI-CGCM3/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120701,v20120510}/ | 1951 | 2050" >> $OUTDIR/namelist
echo "MRI-CGCM3 lgm | /data/CMIP/cmip5/output1/MRI/MRI-CGCM3/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120701,v20120609}/ | 2501 | 2600" >> $OUTDIR/namelist
echo "MRI-CGCM3 historical | /data/CMIP/cmip5/output1/MRI/MRI-CGCM3/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120701,v20120510}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "MRI-CGCM3 1pctCO2 | /data/CMIP/cmip5/output1/MRI/MRI-CGCM3/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120701,v20120510}/ | 1851 | 1990" >> $OUTDIR/namelist

NAME="MIROC-ESM"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "MIROC-ESM piControl | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/piControl/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 1800 | 2429" > $OUTDIR/namelist 
echo "MIROC-ESM midHolocene | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/midHolocene/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 2330 | 2429" >> $OUTDIR/namelist
echo "MIROC-ESM lgm | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/lgm/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 4600 | 4699" >> $OUTDIR/namelist
echo "MIROC-ESM historical | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/historical/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 1850 | 2005" >> $OUTDIR/namelist
echo "MIROC-ESM 1pctCO2 | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/1pctCO2/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 001 | 140" >> $OUTDIR/namelist
echo "MIROC-ESM past1000 | /data/CMIP/cmip5/output1/MIROC/MIROC-ESM/past1000/mon/{atmos,seaIce}/{Amon,OImon}/r1i1p1/v20120710/ | 850 | 1849" >> $OUTDIR/namelist

NAME="MPI-ESM-P"
OUTDIR=/data/aod/cvdp_cmip5/local_output/$NAME
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$NAME.driver.ncl
sed -i "s:Title goes here:$NAME:g" $OUTDIR/$NAME.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$NAME.driver.ncl
echo "MPI-ESM-P piControl | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120602,v20120315,v20120625}/ | 2400 | 3000" > $OUTDIR/namelist 
echo "MPI-ESM-P-p2 piControl | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/piControl/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120602,v20120315,v20120625}/ | 2400 | 3000" > $OUTDIR/namelist 
echo "MPI-ESM-P midHolocene | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120602,v20120315,v20120625}/ | 1850 | 1949" >> $OUTDIR/namelist
echo "MPI-ESM-P-p2 midHolocene | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/midHolocene/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p2/v20120713/ | 1850 | 1949" >> $OUTDIR/namelist
echo "MPI-ESM-P lgm | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120602,v20120315,v20120625}/ | 1850 | 1949" >> $OUTDIR/namelist
echo "MPI-ESM-P-p2 lgm | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/lgm/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p2/v20120713/ | 1850 | 1949" >> $OUTDIR/namelist
echo "MPI-ESM-P historical | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/historical/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120315,v20120625}/ | 1850 | 2005" >> $OUTDIR/namelist
echo "MPI-ESM-P 1pctCO2 | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/1pctCO2/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20111028,v20120315,v20120625}/ | 1850 | 1989" >> $OUTDIR/namelist
echo "MPI-ESM-P past1000 | /data/CMIP/cmip5/output1/MPI-M/MPI-ESM-P/past1000/mon/{atmos,seaIce,ocean}/{Amon,Omon,OImon}/r1i1p1/{v20120625,v20120315,v20111028}/ | 850 | 1849" >> $OUTDIR/namelist

#Create some experiment only ones...
shopt -s extglob
EXPT="past1000"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep past1000 /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="1pctCO2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$EXPT.driver.ncl
grep 1pctCO2 /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist
 
EXPT="abrupt4xCO2"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$EXPT.driver.ncl
grep abrupt4xCO2 /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="piControl"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep piControl /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="midHolocene"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep midHolocene /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="lgm"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep lgm /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="historical"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
echo "C20-Reanalysis | /home/ucfaccb/DATA/obs/C20_Reanal/ | 1871 | 2012" > $OUTDIR/namelist
grep historical /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: >> $OUTDIR/namelist

EXPT="LIA"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep past1000 /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist
sed -i "s:| 850:| 1450:g" $OUTDIR/namelist
sed -i "s:| 851:| 1450:g" $OUTDIR/namelist
sed -i "s:| 862:| 1450:g" $OUTDIR/namelist
sed -i "s:| 1000:| 1450:g" $OUTDIR/namelist
sed -i "s:| 1999:| 1850:g" $OUTDIR/namelist

EXPT="MWP"
OUTDIR=/data/aod/cvdp_cmip5/local_output/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP5 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep past1000 /data/aod/cvdp_cmip5/local_output/!(*.*)/namelist | cut -f2 -d: > $OUTDIR/namelist
sed -i "s:| 850:| 950:g" $OUTDIR/namelist
sed -i "s:| 851:| 950:g" $OUTDIR/namelist
sed -i "s:| 862:| 950:g" $OUTDIR/namelist
sed -i "s:| 1850:| 1250:g" $OUTDIR/namelist
sed -i "s:| 1999:| 1250:g" $OUTDIR/namelist
sed -i "s:| 1849:| 1250:g" $OUTDIR/namelist


#!/bin/bash

#expt_names="piControl abrupt4xCO2 1pctCO2 lgm midHolocene lig127k"
expt_names="abrupt4xCO2 1pctCO2"
CVDP_DIR=`pwd`
ESGF_DIR="/data/CMIP/curated_ESGF_replica"
CVDP_OUTDIR="/data/aod/cvdp_PVar_kira/"

gcms=`ls -I README $ESGF_DIR/`
for expt in $expt_names
do
  #first set up some run scripts etc for the experiment
  OUTDIR=$CVDP_OUTDIR/output.$expt
  mkdir -p $OUTDIR
  cp driver.ncl $OUTDIR/$expt.driver.ncl
  sed -i "s:output/:$OUTDIR/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:ncl_scripts/:$CVDP_DIR/ncl_scripts/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:Title goes here:$expt:g" $OUTDIR/$expt.driver.ncl
  sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
  sed -i "s:climo_syear = 1971:climo_syear = -49:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$expt.driver.ncl
  rm -f $OUTDIR/namelist

  # And now start filling the namelist
  cd $ESGF_DIR
  for gcm in $gcms
  do
    model=$gcm
    if [ -d "$ESGF_DIR/$gcm/$expt" ]; then
      cd $ESGF_DIR/$gcm/$expt
      ncfiles=*.nc
      let start_yr=9999
      let end_yr=1
      for ncfile in $ncfiles
      do
       yr_str=${ncfile##*_}
       this_start_yr=`echo $yr_str | cut -c-4`
       this_end_yr=`echo ${yr_str##*-} | cut -c-4`
       #echo model: $this_start_yr and $this_end_yr
       if [ $this_start_yr -lt $start_yr ] ; then start_yr=$((10#$this_start_yr)); fi
       if [ $this_end_yr -gt $end_yr ] ; then end_yr=$((10#$this_end_yr)); fi
      done
      let length=$((10#$end_yr))-$((10#$start_yr))
      if [ $length -gt 10 ]; then
       handedit="$model+$expt"
        case $handedit in
         "FGOALS-g2+historical") #some files go 1850-2005, others 1900-2014     
        	  echo "$model $expt | $ESGF_DIR/$gcm/$expt/ | 1956 | 2005" >> $OUTDIR/namelist
           ;;
         *) #Default option
           if [ $length -lt 49 ]; then 
        	    echo "$model $expt | $ESGF_DIR/$gcm/$expt/ | $start_yr | $end_yr" >> $OUTDIR/namelist
           else  
             let start_50yr=$((10#$end_yr))-49   
        	    echo "$model $expt | $ESGF_DIR/$gcm/$expt/ | $start_50yr | $end_yr" >> $OUTDIR/namelist
           fi
           ;;
        esac    
      fi
    fi
  done
 
  #Launch the executables
  cd $OUTDIR
  ncl -n $expt.driver.ncl >& out.log &  

  cd $CVDP_DIR
done
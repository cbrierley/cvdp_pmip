#!/bin/bash

expt_names="piControl abrupt4xCO2 1pctCO2 historical lgm midHolocene past1000 rcp85"
CVDP_DIR=`pwd`
if [[ $(hostname) = *.badc.* ]] || [[ $(hostname) = *.rl.* ]]; then
  CMIP5DIR="/badc/cmip5/data/cmip5/output1"
  CVDP_OUTDIR="/group_workspaces/jasmin2/ncas_generic/users/ucfaccb/cvdp_cmip5/"
else
  CMIP5DIR="/data/CMIP/cmip5/output1"
  CVDP_OUTDIR="/data/aod/cvdp_cmip5/"
fi
switchout="{atmos,seaIce,ocean}/{Amon,Omon,OImon}" #substitution variable for later

for expt in $expt_names
do
  #first set up some run scripts etc for the experiment
  OUTDIR=$CVDP_OUTDIR/output.$expt
  mkdir -p $OUTDIR
  cp driver.ncl $OUTDIR/$expt.driver.ncl
  sed -i "s:output/:$OUTDIR/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:ncl_scripts/:$CVDP_DIR/ncl_scripts/:g" $OUTDIR/$expt.driver.ncl
  sed -i "s:Title goes here:$expt:g" $OUTDIR/$expt.driver.ncl
  if [ $expt == "1pctCO2" ] #NOTE: OPTIONS FOR OTHER RUNS NEEDS TO BE SET AS WELL
  then
    sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$expt.driver.ncl
  fi  
  if [ $expt == "historical" ] #NOTE: OPTIONS FOR OTHER RUNS NEEDS TO BE SET AS WELL
  then
    sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
  fi  
  if [ $expt == "rcp85" ] #NOTE: OPTIONS FOR OTHER RUNS NEEDS TO BE SET AS WELL
  then
    sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_syear = 1971:climo_syear = 2070:g" $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_eyear = 2000:climo_eyear = 2099:g" $OUTDIR/$expt.driver.ncl
  fi  
  if [ $expt == "abrupt4xCO2" ] #NOTE: OPTIONS FOR OTHER RUNS NEEDS TO BE SET AS WELL
  then
    sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$expt.driver.ncl
    sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$expt.driver.ncl
  fi  

  rm -f $OUTDIR/namelist

  # And now start filling the namelist
  cd $CMIP5DIR
  datasets=`ls -d */*/$expt/mon/atmos/Amon/r*`
  for dataset in $datasets
  do
    model=`echo $dataset | cut -d/ -f 2`
    ensmem=`echo $dataset | cut -d/ -f 7`
    model_dirstr=`echo "${dataset/atmos\/Amon/$switchout}""/latest/{ts/,tas/,psl/,pr/,msftmyz/,snd/,sic/,}"`
    #The harder bits are finding the correct years. NOTE THIS MAY FAIL FOR COMPLETELY EMPTY DATASETS
    ncfiles=`eval ls $model_dirstr*.nc 2> /dev/null`
    let start_yr=9999
    let end_yr=1
    for ncfile_wdir in $ncfiles
    do
      ncfile=${ncfile_wdir##*/}	
      yr_str=`echo $ncfile | cut -d_ -f 6`
      this_start_yr=`echo $yr_str | cut -d- -f 1 | cut -c-4`
      this_end_yr=`echo $yr_str | cut -d- -f 2 | cut -d. -f 1 | cut -c-4`
      if [ $this_start_yr -lt $start_yr ]; then start_yr=$this_start_yr; fi
      if [ $this_end_yr -gt $end_yr ]; then end_yr=$this_end_yr; fi
    done
    let length=$((10#$end_yr))-$((10#$start_yr))
    if [ $length -gt 10 ]; then
	echo "$model $ensmem $expt | $CMIP5DIR/$model_dirstr | $start_yr | $end_yr" >> $OUTDIR/namelist
    fi
  done
  cd $CVDP_DIR
done

#If on Jasmin then submit these as batch jobs
if [[ $(hostname) = *.badc.* ]] || [[ $(hostname) = *.rl.* ]]; then
  for expt in $expt_names
  do
      cd $CVDP_OUTDIR/output.$expt
      rm $expt.driver.log
      echo '#!/bin/bash' > $expt.cvdp.bsub
      echo "#BSUB -J $expt.cvdp" >>  $expt.cvdp.bsub
      echo "#BSUB -q par-single" >>  $expt.cvdp.bsub
      echo "#BSUB -oo $expt.cvdp.out" >>  $expt.cvdp.bsub 
      echo "#BSUB -eo $expt.cvdp.err"  >>  $expt.cvdp.bsub
      echo "#BSUB -W 48:00" >>  $expt.cvdp.bsub
      echo "#BSUB -n 4" >>  $expt.cvdp.bsub
      echo " " >>  $expt.cvdp.bsub
      echo "ncl -n $expt.driver.ncl" >>  $expt.cvdp.bsub   
      bsub < $expt.cvdp.bsub
  done
fi

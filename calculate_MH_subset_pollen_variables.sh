#!/bin/bash

CVDP_DIR=`pwd`
ESGF_DIR="/data/CMIP/curated_ESGF_replica"
gcm_names=`ls -I README $ESGF_DIR | sort | uniq`

for gcm in $gcm_names
do 
  cd $ESGF_DIR/$gcm
  if [ -d $ESGF_DIR/$gcm/midHolocene-cal-adj ]; then
    echo Starting on $gcm
    chmod 755 midHolocene-cal-adj #make run directory writable
    mkdir -p midHolocene-cal-adj/preprocessed
    cd midHolocene-cal-adj/preprocessed
    #create change in Mean Annual Temperature 
    ncra --mro -O -d time,,,12,12 ../../piControl/tas_Amon*.nc MATs_PI_$gcm.nc
    ncra -O MATs_PI_$gcm.nc MAT_PI_$gcm.nc 
    ncra --mro -O -d time,,,12,12 ../../midHolocene/tas_Amon*.nc MATs_MH_$gcm.nc 
    ncra -O MATs_MH_$gcm.nc MAT_MH_$gcm.nc
    ncdiff -O MAT_MH_$gcm.nc MAT_PI_$gcm.nc dMAT_MH-PI_$gcm.nc
    ncrename -v tas,dMAT dMAT_MH-PI_$gcm.nc
    ncatted -a long_name,dMAT,o,c,"Change in mean annual temperature" dMAT_MH-PI_$gcm.nc
    rm MAT_MH_$gcm.nc MATs_MH_$gcm.nc MAT_PI_$gcm.nc MATs_PI_$gcm.nc
    #create change in Mean Annual Precip
    ncra --mro -O -d time,,,12,12 ../../piControl/pr_Amon*.nc MAPs_PI_$gcm.nc
    ncra -O MAPs_PI_$gcm.nc MAP_PI_$gcm.nc 
    ncra --mro -O -d time,,,12,12 ../../midHolocene/pr_Amon*.nc MAPs_MH_$gcm.nc 
    ncra -O MAPs_MH_$gcm.nc MAP_MH_$gcm.nc
    ncdiff -O MAP_MH_$gcm.nc MAP_PI_$gcm.nc dpr_MH-PI_$gcm.nc
    ncap2 -O -v -s "dMAP=pr*84600.*365." dpr_MH-PI_$gcm.nc dMAP_MH-PI_$gcm.nc
    ncatted -a units,dMAP,o,c,"mm" dMAP_MH-PI_$gcm.nc
    ncatted -a long_name,dMAP,o,c,"Change in mean annual precipitation" dMAP_MH-PI_$gcm.nc
    rm MAP_MH_$gcm.nc MAPs_MH_$gcm.nc MAP_PI_$gcm.nc MAPs_PI_$gcm.nc dpr_MH-PI_$gcm.nc
    #create change in Mean Temperature of Coldest Month 
    ncra -y min --mro -O -d time,,,12,12 ../../piControl/tas_Amon*.nc MTCOs_PI_$gcm.nc
    ncra MTCOs_PI_$gcm.nc MTCO_PI_$gcm.nc 
    ncra -y min --mro -O -d time,,,12,12 ../tas_Amon*.nc MTCOs_MH_$gcm.nc 
    ncra MTCOs_MH_$gcm.nc MTCO_MH_$gcm.nc
    ncdiff MTCO_MH_$gcm.nc MTCO_PI_$gcm.nc dMTCO_MH-PI_$gcm.nc
    ncrename -v tas,dMTCO dMTCO_MH-PI_$gcm.nc
    ncatted -a long_name,dMTCO,o,c,"Change in mean temperature of the coldest month" dMTCO_MH-PI_$gcm.nc
    rm MTCO_MH_$gcm.nc MTCOs_MH_$gcm.nc MTCO_PI_$gcm.nc MTCOs_PI_$gcm.nc
    #create change in Mean Temperature of Warmest Month 
    ncra -y max --mro -O -d time,,,12,12 ../../piControl/tas_Amon*.nc MTWAs_PI_$gcm.nc
    ncra MTWAs_PI_$gcm.nc MTWA_PI_$gcm.nc 
    ncra -y max --mro -O -d time,,,12,12 ../tas_Amon*.nc MTWAs_MH_$gcm.nc 
    ncra MTWAs_MH_$gcm.nc MTWA_MH_$gcm.nc
    ncdiff MTWA_MH_$gcm.nc MTWA_PI_$gcm.nc dMTWA_MH-PI_$gcm.nc
    ncrename -v tas,dMTWA dMTWA_MH-PI_$gcm.nc
    ncatted -a long_name,dMTWA,o,c,"Change in mean temperature of the warmest month" dMTWA_MH-PI_$gcm.nc
    rm MTWA_MH_$gcm.nc MTWAs_MH_$gcm.nc MTWA_PI_$gcm.nc MTWAs_PI_$gcm.nc
    #Combine into a single file
    ncks -A -v dMAP dMAP_MH-PI_$gcm.nc dMAT_MH-PI_$gcm.nc
    ncks -A -v dMTCO dMTCO_MH-PI_$gcm.nc dMAT_MH-PI_$gcm.nc
    ncks -A -v dMTWA dMTWA_MH-PI_$gcm.nc dMAT_MH-PI_$gcm.nc
    chmod 644 $gcm\_midHolocene_change_in_pollen_variables.nc
    mv dMAT_MH-PI_$gcm.nc $gcm\_midHolocene_change_in_pollen_variables.nc
    rm dMAP_MH-PI_$gcm.nc dMTWA_MH-PI_$gcm.nc dMTCO_MH-PI_$gcm.nc
    chmod 444 *.nc
    cd ../..
    chmod 555 midHolocene-cal-adj #make run directory read-only
  fi
done

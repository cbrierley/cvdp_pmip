#written in bash

EXPT="1pctCO2"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$EXPT.driver.ncl
grep 1pctCO2 /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: > $OUTDIR/namelist
 
EXPT="abrupt-4xCO2"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_syear = 1971:climo_syear = -30:g" $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_eyear = 2000:climo_eyear = 0:g" $OUTDIR/$EXPT.driver.ncl
grep abrupt-4xCO2 /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="piControl"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep piControl /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="midHolocene"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep midHolocene /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="lig127k"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
grep lig127k /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: > $OUTDIR/namelist

EXPT="historical"
OUTDIR=/data/aod/cvdp_cmip6/output.$EXPT
mkdir $OUTDIR
sed "s:output/:$OUTDIR/:g" driver.ncl > $OUTDIR/$EXPT.driver.ncl
sed -i "s:Title goes here:CMIP6 $EXPT:g" $OUTDIR/$EXPT.driver.ncl
sed -i 's:zp = "ncl_scripts/":zp = "/home/ucfaccb/Documents/local_repos/cvdp_pmip/ncl_scripts/":g' $OUTDIR/$EXPT.driver.ncl
sed -i 's:opt_climo = "Full":opt_climo = "Custom":g' $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_syear = 1971:climo_syear = 1981:g" $OUTDIR/$EXPT.driver.ncl
sed -i "s:climo_eyear = 2000:climo_eyear = 2010:g" $OUTDIR/$EXPT.driver.ncl
echo "C20-Reanalysis | /home/ucfaccb/DATA/obs/C20_Reanal/ | 1871 | 2012" > $OUTDIR/namelist
grep historical /data/aod/cvdp_cmip6/all_cmip6_together/namelist | cut -f2 -d: >> $OUTDIR/namelist